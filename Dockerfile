FROM debian
VOLUME /var/lib/docker
RUN apt update && apt install -y iptables wget kmod curl
RUN wget https://download.docker.com/linux/static/stable/x86_64/docker-18.09.3.tgz && tar xvfz docker-18.09.3.tgz && mv docker/* /usr/local/bin/
RUN curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/v0.30.0/skaffold-linux-amd64 && chmod +x skaffold && mv skaffold /usr/local/bin
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x kubectl && mv kubectl /usr/local/bin
RUN wget https://github.com/rancher/k3s/releases/download/v0.5.0/k3s && chmod +x k3s && mv k3s /usr/local/bin
